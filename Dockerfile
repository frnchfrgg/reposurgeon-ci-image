FROM golang:1.13-buster

RUN apt-get update -qy && apt-get install -qy --no-install-recommends \
    asciidoc \
    asciidoctor \
    cvs \
    cvs-fast-export \
    mercurial \
    python2.7 \
    python3 \
    subversion \
    time \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*
